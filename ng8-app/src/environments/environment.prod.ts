/*
This is the default environment that is loaded when app is started in PROD mode 
for example: after being built with "ng build --prod", and copying everything from the "dist" folder to a web server
*/
export const environment = {
  /*
  production:boolean is a standard field in the environment object
  */
  production: true,

  name: "Production",
  /*
  any other fields of any types (including sub-objects) can be added to the environment
  */
  backEnd: {
    rootUrl: "http://localhost:8080/ang/back-end",
    endpointUrls: {
      networkElementSearch: "/network-element/search",
      networkElementGetById: "/network-element/get-by-id",
      networkElementCreate: "/network-element/create",
      networkElementUpdate: "/network-element/update"
    }
  },

  frontEnd: {
    //these are date format tokens that are specific to the moment.js library
    //(and which are somewhat different from the ones regularly used in other languages.
    //for example Java and Angular 8's date filter would express this format like this: 'yyyy/MM/dd HH:mm:ss')
    uiDateFormat: "YYYY/MM/DD HH:mm:ss"
  }
};
