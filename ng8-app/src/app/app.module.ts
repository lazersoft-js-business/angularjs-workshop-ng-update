import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NetworkElementListComponent } from './components/network-element-list/network-element-list.component';

import { MatSliderModule, MatIconModule } from '@angular/material';
import 'hammerjs';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NetworkElementEditComponent } from './components/network-element-edit/network-element-edit.component';
import { Routes, RouterModule } from '@angular/router';

import { HttpClientModule} from '@angular/common/http';

/*
    BrowserAnimationsModule vs NoopAnimationsModule (also see comment bellow, on the import of these actual modules into the app module)
    If BrowserAnimationsModule is imported, form inputs made with Angular Material library will have small animations (label will slide up and down on text input fields, slider widget will slide from 0 to current value when initialzied, etc, etc)
    If NoopAnimationsModule is imported instead, no sliding or any kind of animations will happen
*/
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainMenuComponent } from './components/main-menu/main-menu.component';

import { SortableTableHeaderComponent } from './components/sortable-table-header/sortable-table-header.component';
import { DebugComponent } from './components/debug/debug.component';
import { NetworkElementEditWithValidationComponent } from './components/network-element-edit-with-validation/network-element-edit-with-validation.component';

const appRoutes: Routes = [
  //apparently the url (route) parameters can no longer be declared as optional. 
  //as such, the ne edit route needs to be declared twice, once without any ne ID param (for Create NE scenario),
  //and once with (mandatory, now), /:ID url param, for editing an existing NE:
  { path: 'network-element-edit', component: NetworkElementEditComponent },
  { path: 'network-element-edit/:ID', component: NetworkElementEditComponent },

  { path: 'network-element-edit-with-validation', component: NetworkElementEditWithValidationComponent },
  { path: 'network-element-edit-with-validation/:ID', component: NetworkElementEditWithValidationComponent },

  { path: 'network-element-list', component: NetworkElementListComponent },

  { path: 'debug', component: DebugComponent },

  { path: '', redirectTo: '/network-element-list', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    NetworkElementListComponent,
    NetworkElementEditComponent,
    MainMenuComponent,
    SortableTableHeaderComponent,
    DebugComponent,
    NetworkElementEditWithValidationComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    MatSliderModule,
    MatIconModule,
    NgbModule,
    HttpClientModule,

    /*
    BrowserAnimationsModule vs NoopAnimationsModule, see comment above on import if these modules' files
     */
    // BrowserAnimationsModule,
    NoopAnimationsModule,

    FormsModule,

    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
