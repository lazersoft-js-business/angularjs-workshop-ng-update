import { Sort } from './sort';


export interface NetworkElementSearchRequest {
    from: Number,
    size: Number,
    sort: Sort
}