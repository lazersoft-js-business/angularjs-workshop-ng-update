import { NetworkElementForUI } from './network-element-for-ui';

export class NetworkElementSearchResponseForUI {
    results: NetworkElementForUI[];
    totalHits: number;
}