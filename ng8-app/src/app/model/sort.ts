import { SortDirection } from './sort-direction';

export interface Sort {
    field: String,
    order: SortDirection
}