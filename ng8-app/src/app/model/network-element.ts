/*
This one is created as a class (and not an interface, like the others) because we need to instantiate it (when user creates new NE)
Interfaces don't have empty constructors
*/
export class NetworkElement {
    ID: number;
    NAME: string;
    IP: string;
    PRODUCT_FAMILY: string;
    PRODUCT_NAME: string;
    GROUP: string;
    SLOTS: number;
    LAST_UPDATE_TIMESTAMP: string;
}