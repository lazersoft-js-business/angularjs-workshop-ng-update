export interface Column {
    readonly field: string,
    readonly displayLabel: string,
    readonly sortable: boolean
}