import { NetworkElement } from './network-element';

export interface NetworkElementSearchResponse {
    results: NetworkElement[],
    totalHits: number
}