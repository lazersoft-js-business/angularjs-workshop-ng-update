export interface MainMenuEntry {
    label: String,
    route: String
}