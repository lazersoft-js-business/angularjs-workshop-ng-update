import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Column } from 'src/app/model/column';
import { Sort } from '../../model/sort';
import { SortDirection } from '../../model/sort-direction';

@Component({
  selector: '[app-sortable-table-header]',
  templateUrl: './sortable-table-header.component.html',
  styleUrls: ['./sortable-table-header.component.css']
})
export class SortableTableHeaderComponent implements OnInit {

  /*
  The @Input() annotation on the "columns" parameter means that this component exposes this argument for data receiving:
  when the component is inserted in the HTML template of another (now, parent) component, the parent component can specify a value (of type Column[]),
  in the "columns" attribute for this component to receive
  */
  @Input()
  columns:Column[];

  @Input()
  sort:Sort;
  
  /*
  The @Output() annotation on the "sortChanged" parameter means that this component exposes this argument for data sending:
  when the component is inserted in the HTML template of another (now, parent) component, the parent component can specify a function (in format "functionName($event)"),
  in the "sortChanged", which will be called when this component emits an event
  */
  @Output()
  sortChanged: EventEmitter<Sort> = new EventEmitter();

  //expose SortDirection enum values to tempalte:
  sortDirectionAsc = SortDirection.Asc;
  sortDirectionDesc = SortDirection.Desc;

  constructor() { }

  updateSortColumn(column:Column) {
    if(this.sort.field === column.field) {
			//when the user has clicked again on the currently sorting column, change its direction:
			this.sort.order = this.sort.order == SortDirection.Asc ? SortDirection.Desc : SortDirection.Asc;
		} else {
			//we have a new sort column, set it to asc:
			this.sort.field = column.field;
			this.sort.order = SortDirection.Asc;			
    }

    //emit data about the new sort state, using the sortChanged EventEmitter, for all who listen for it:
    this.sortChanged.emit(this.sort);
  }

  ngOnInit() {
  }

}
