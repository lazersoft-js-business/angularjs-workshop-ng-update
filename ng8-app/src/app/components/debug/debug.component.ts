import { Component, OnInit } from '@angular/core';

//make sure this import is exactly like this (on the 'src/environments/environment')
//only this file will dinamically be populated with the values from the other environment.xxx.ts files, depending on the selected configuation:
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.css']
})
export class DebugComponent implements OnInit {

  env;

  constructor() { }

  ngOnInit() {
    this.env = environment;
  }

}
