import { Component, OnInit, Input } from '@angular/core';

import { NetworkElementService } from '../../services/network-element.service';
import { NetworkElementSearchRequest } from '../../model/network-element-search-request';
import { Observable } from 'rxjs';
import { NetworkElement } from '../../model/network-element';
import { NetworkElementSearchResponse } from '../../model/network-element-search-response';
import { Column } from 'src/app/model/column';
import { Sort } from 'src/app/model/sort';
import { SortDirection } from 'src/app/model/sort-direction';

@Component({
  selector: 'app-network-element-list',
  templateUrl: './network-element-list.component.html',
  styleUrls: ['./network-element-list.component.css']
})
export class NetworkElementListComponent implements OnInit {

  columns: Column[] = [
    { field: "ID", displayLabel: "Id", sortable: true },
    { field: "NAME", displayLabel: "Name", sortable: true },
    { field: "PRODUCT_FAMILY", displayLabel: "Product Family", sortable: true },
    { field: "PRODUCT_NAME", displayLabel: "Product Name", sortable: true },
    { field: "IP", displayLabel: "IP", sortable: true },
    { field: "SLOTS", displayLabel: "Slots", sortable: true },
    { field: "GROUP", displayLabel: "Group Configuration", sortable: true },
    { field: "LAST_UPDATE_TIMESTAMP", displayLabel: "Last Updated", sortable: true },
    { field: "actions", displayLabel: "Actions", sortable: false }
  ];
  
  networkElementSearchRequest: NetworkElementSearchRequest;

  networkElements: NetworkElement[];
  totalHits: number;

  currentPage: number = 1;
  //specifying the type of a variable is optional:
  pageSize = 5;

  initialSort: Sort = {
    field: "ID",
    order: SortDirection.Asc
  };

  private sort = this.initialSort;

  constructor(private networkElementService: NetworkElementService) {

  }

  loadDataForCurrentPage() {
    console.log("loadDataForCurrentPage this.currentPage=" + this.currentPage);
    this.updateNetworkElementSearchRequest();

    var searchResultObservable: Observable<NetworkElementSearchResponse> = this.networkElementService.search(this.networkElementSearchRequest);
    searchResultObservable.subscribe((networkElementSearchResponse: NetworkElementSearchResponse) => {
      //in ng8 TypeScript, "this" reffers to the encompassing instance (in this care the NetworkElementListComponent instance
      //(NOT the local lambda/closure/function, like in ng1 JS):
      this.networkElements = networkElementSearchResponse.results;
      this.totalHits = networkElementSearchResponse.totalHits;
    })
  }

  onPageSizeChanged(event) {
    this.pageSize = event.value;
    this.currentPage = 1;
    this.loadDataForCurrentPage();
  }

  onSortChanged(newSort) {
    this.sort = newSort;		
		this.currentPage = 1;
		this.loadDataForCurrentPage();
  }

  private updateNetworkElementSearchRequest() {
    this.networkElementSearchRequest = {
      from: (this.currentPage - 1) * this.pageSize,
      size: this.pageSize,
      sort: this.sort
    }
  }

  ngOnInit() {
    //this works like the regular console object in JS, outputting to the browser's "Console" tab (in "Developer tools"):
    console.log("NetworkElementListComponent.ngOnInit");
    this.loadDataForCurrentPage();
  }

}
