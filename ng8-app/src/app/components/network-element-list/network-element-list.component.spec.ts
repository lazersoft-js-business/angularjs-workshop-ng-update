import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkElementListComponent } from './network-element-list.component';

describe('NetworkElementListComponent', () => {
  let component: NetworkElementListComponent;
  let fixture: ComponentFixture<NetworkElementListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetworkElementListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkElementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /*
  replace "it" with "xit" to eXclude a spect from execution
  */
  // it('should create', () => {
  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
