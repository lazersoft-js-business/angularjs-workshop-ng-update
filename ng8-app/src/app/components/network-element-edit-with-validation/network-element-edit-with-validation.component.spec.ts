import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkElementEditWithValidationComponent } from './network-element-edit-with-validation.component';

describe('NetworkElementEditWithValidationComponent', () => {
  let component: NetworkElementEditWithValidationComponent;
  let fixture: ComponentFixture<NetworkElementEditWithValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetworkElementEditWithValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkElementEditWithValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
