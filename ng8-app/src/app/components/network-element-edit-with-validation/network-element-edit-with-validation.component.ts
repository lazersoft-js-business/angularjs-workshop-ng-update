import { Component, OnInit } from '@angular/core';
import { NetworkElementForUI } from 'src/app/model/network-element-for-ui';
import { ActivatedRoute, Router } from '@angular/router';
import { NetworkElementService } from 'src/app/services/network-element.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ipFormValidator } from '../../services/ip-form-validator';
import * as moment from 'moment';
import { dateStringValidator } from '../../services/date-string-validator';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-network-element-edit-with-validation',
  templateUrl: './network-element-edit-with-validation.component.html',
  styleUrls: ['./network-element-edit-with-validation.component.css']
})
export class NetworkElementEditWithValidationComponent implements OnInit {

  operationTypeDisplay: string;

  productFamilies = ["ip10", "ip20"];

  productNamesByFamily;
  groupsByFamily;

  neForUI: NetworkElementForUI;

  networkElementForm: FormGroup;
  //the form control for each field in the form needs to also be exposed as a global variable in the component
  //so it can be addressed in the HTML template (for example to get current validation error message, and display it, etc)
  name: FormControl;
  ip: FormControl;
  productFamily: FormControl;
  productName: FormControl;
  group: FormControl;
  slots: FormControl;
  lastUpdateTimestamp: FormControl;

  humanReadableErrorMessage;

  private formDateFormat;
  private selectedId: number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private networkElementService: NetworkElementService) {

    //init networkElement instance in constructor, not in ngOnInit, so ngModel bindings in template won't complain about it being null:  
    this.neForUI = new NetworkElementForUI();

    this.formDateFormat = environment.frontEnd.uiDateFormat;
  }

  ngOnInit() {
    this.initSelectOptions();
    this.initNetworkElementForUI();
  }

  onNEFormSubmit() {
    //we assume that if we got here, the form is valid both at field-level, and globally (various multi-field crossvalidation)
    this.neForUI.GROUP = this.group.value;
    this.neForUI.IP = this.ip.value;
    this.neForUI.LAST_UPDATE_TIMESTAMP = this.lastUpdateTimestamp.value;
    this.neForUI.NAME = this.name.value;
    this.neForUI.PRODUCT_FAMILY = this.productFamily.value;
    this.neForUI.PRODUCT_NAME = this.productName.value;
    this.neForUI.SLOTS = this.slots.value;

    var responseObservable: Observable<any> = this.networkElementService.persistNetworkElementForUI(this.neForUI);
    responseObservable
      .subscribe(

        //do something when everything worked out ok.
        //res represents the HTTP response body (marshalled to an object. In this particular case, the body of the response is empty)
        (res: Object) => {
          console.log("NE persist HTTP call executed ok");
          this.humanReadableErrorMessage = null;
          this.router.navigateByUrl("/network-element-list");
        },

        //do something when there was an error with the HTTP call:
        (err: HttpErrorResponse) => {
          this.humanReadableErrorMessage = "NE persist HTTP call executed with error: statusText=" + err.statusText + ", HTTP Status: " + err.status;
        }
      );
  }

  private initSelectOptions() {
    this.productNamesByFamily = [];
    this.productNamesByFamily["ip10"] = ["ip10c", "ip10e", "ip10g"];
    this.productNamesByFamily["ip20"] = ["ip20a", "ip20f", "ip20e", "ip20s", "ip20c", "ip20n"];

    this.groupsByFamily = [];
    this.groupsByFamily["ip10"] = ["1+0", "HSB", "MR"];
    this.groupsByFamily["ip20"] = ["1+0", "ABC", "RPG"];
  }

  private initNetworkElementForUI() {
    this.selectedId = this.route.snapshot.paramMap.has("ID") ? Number(this.route.snapshot.paramMap.get("ID")) : null;
    if (this.selectedId == null) {
      this.operationTypeDisplay = "Create";
      //if we're in "Create" mode, init form right way (no data to pre-populate the form with)
      this.initForm();
    } else {
      this.operationTypeDisplay = "Edit";
      this.networkElementService.getForUIById(this.selectedId).subscribe(neForUI => {
        this.neForUI = neForUI;
        //if we're in "Edit" mode, first wait for the data to arrive from back-end, and then init the form (prepopulated with the back-end data):
        this.initForm();
      });
    }
  }

  private initForm() {
    this.name = new FormControl(this.neForUI.NAME,
      [
        //standard validators
        Validators.required,
        Validators.minLength(4)
      ]
    );

    this.ip = new FormControl(this.neForUI.IP,
      [
        //custom validator (that also checks for "required")
        //this particular custom validator was implemented as a separate function, in it's own file (so it can be reused in other views)
        //alternatively, this could just have been a private function in this component (this.ipFormValidator)
        ipFormValidator
      ]
    );

    this.productFamily = new FormControl(this.neForUI.PRODUCT_FAMILY,
      [
        Validators.required
      ]
    );
    /*
    for product name and group, when changing the product family (in the UI input), it's not sufficient to update their lists of options (in the HTML template)
    We need to also update their form controls, to set them to the first option in the new list of options
    this is done programatically here, when initializing the productName FormControl, by registering a change listener on it:
    */
    this.productFamily.valueChanges.subscribe((value) => {
      this.group.setValue("1+0");

      if (value == null) {
        this.productName.setValue(null);
      } else {
        this.productName.setValue(this.productNamesByFamily[value][0]);
      }
    });

    //this one has no validators:
    this.productName = new FormControl(this.neForUI.PRODUCT_NAME);
    this.group = new FormControl(this.neForUI.GROUP);

    this.slots = new FormControl(this.neForUI.SLOTS,
      [
        //mixed standard and custom validators:
        Validators.required,
        Validators.min(1),
        Validators.max(16),
        //custom validator from private function in this component:
        this.validateSlotsInteger
      ]
    );

    this.slots = new FormControl(this.neForUI.SLOTS,
      [
        //mixed standard and custom validators:
        Validators.required,
        Validators.min(1),
        Validators.max(16),
        //custom validator from private function in this component:
        this.validateSlotsInteger
      ]
    );

    this.lastUpdateTimestamp = new FormControl(this.neForUI.LAST_UPDATE_TIMESTAMP,
      [
        Validators.required,
        dateStringValidator(this.formDateFormat)
      ]
    );

    this.networkElementForm = new FormGroup(
      {
        "name": this.name,
        "ip": this.ip,
        "productFamily": this.productFamily,
        "productName": this.productName,
        "group": this.group,
        "slots": this.slots,
        "lastUpdateTimestamp": this.lastUpdateTimestamp
      },

      // global form validators can be declared here, for the entire form:   
      {

        validators: [
          this.crossValidateProductFamilyAndSlots
        ]
      }
    );
  }

  //bizarelly, this validator will allow values such as "12.0" and even "12.":
  private validateSlotsInteger(control: FormControl) {
    var rawInputValue: string = control.value;
    var numericInputValue: number = Number.parseFloat(rawInputValue);

    if (Number.isInteger(numericInputValue)) {
      return null;
    } else {
      return {
        integer: true
      }
    }
  }

  private crossValidateProductFamilyAndSlots(form: FormGroup) {
    if (form.get("productFamily").invalid || form.get("slots").invalid) {
      // any of the fields have specific validation error, return null (valid) here, as they'll display their own error message, 
      // and also prevent the form from being submitted
      return null;
    }

    var productFamily: string = form.get("productFamily").value;
    var slots: number = Number.parseInt(form.get("slots").value);

    if (productFamily == "ip10" && slots > 8) {
      return {
        ip10Max8Slots: true
      }
    }

    return null;
  }
}
