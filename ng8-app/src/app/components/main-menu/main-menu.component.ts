import { Component, OnInit, Input } from '@angular/core';
import { MainMenuEntry } from 'src/app/model/main-menu-entry';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

  @Input()
  entries:MainMenuEntry[];

  constructor() { 
    console.log("Main Menu entries received: " + this.entries);
  }

  ngOnInit() {
  }

}
