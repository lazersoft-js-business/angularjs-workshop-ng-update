import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NetworkElementService } from 'src/app/services/network-element.service';
import { NetworkElement } from 'src/app/model/network-element';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-network-element-edit',
  templateUrl: './network-element-edit.component.html',
  styleUrls: ['./network-element-edit.component.css']
})
export class NetworkElementEditComponent implements OnInit {

  operationTypeDisplay: string;

  productFamilies = ["ip10", "ip20"];

  productNamesByFamily;
  groupsByFamily;

  formDateFormat;

  networkElement: NetworkElement;

  humanReadableErrorMessage: string

  private selectedId: number;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private networkElementService: NetworkElementService) {

    //init networkElement instance in constructor, not in ngOnInit, so ngModel bindings in template won't complain about it being null:  
    this.networkElement = new NetworkElement();
    this.formDateFormat = environment.frontEnd.uiDateFormat;
  }

  persistNetworkElement() {
    this.networkElement.LAST_UPDATE_TIMESTAMP = moment(this.networkElement.LAST_UPDATE_TIMESTAMP, this.formDateFormat, true).format("YYYY-MM-DDTHH:mm:ss");

    var responseObservable: Observable<any> = this.networkElementService.persistNetworkElement(this.networkElement);
    responseObservable
      .subscribe(

        //do something when everything worked out ok.
        //res represents the HTTP response body (marshalled to an object. In this particular case, the body of the response is empty)
        (res: Object) => {
          console.log("NE persist HTTP call executed ok");
          this.humanReadableErrorMessage = null;
          this.router.navigateByUrl("/network-element-list");
        },

        //do something when there was an error with the HTTP call:
        (err: HttpErrorResponse) => {
          this.humanReadableErrorMessage = "NE persist HTTP call executed with error: statusText=" + err.statusText + ", HTTP Status: " + err.status;
        }
      );
  }

  isDateStringValid(dateStr: string): boolean {
    return moment(dateStr, this.formDateFormat, true).isValid();
  }

  ngOnInit() {
    this.productNamesByFamily = [];
    this.productNamesByFamily["ip10"] = ["ip10c", "ip10e", "ip10g"];
    this.productNamesByFamily["ip20"] = ["ip20a", "ip20f", "ip20e", "ip20s", "ip20c", "ip20n"];

    this.groupsByFamily = [];
    this.groupsByFamily["ip10"] = ["1+0", "HSB", "MR"];
    this.groupsByFamily["ip20"] = ["1+0", "ABC", "RPG"];

    this.route.queryParams.subscribe(params => {
        this.selectedId = params['id'];
    });
    
    if (this.selectedId == null) {
      this.operationTypeDisplay = "Create";
      this.networkElement.LAST_UPDATE_TIMESTAMP = moment().format(this.formDateFormat);
    } else {
      this.operationTypeDisplay = "Edit";
      this.networkElementService.getById(this.selectedId).subscribe(networkElement => {
        this.networkElement = networkElement;
        this.networkElement.LAST_UPDATE_TIMESTAMP = moment(this.networkElement.LAST_UPDATE_TIMESTAMP).format(this.formDateFormat);
      });
    }

  }

}
