import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkElementEditComponent } from './network-element-edit.component';

describe('NetworkElementEditComponent', () => {
  let component: NetworkElementEditComponent;
  let fixture: ComponentFixture<NetworkElementEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetworkElementEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkElementEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
