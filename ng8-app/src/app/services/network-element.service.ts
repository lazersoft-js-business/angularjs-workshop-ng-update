import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NetworkElementSearchRequest } from '../model/network-element-search-request';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { NetworkElement } from '../model/network-element';
import { NetworkElementSearchResponse } from '../model/network-element-search-response';
import { SortDirection } from '../model/sort-direction';
import { map } from 'rxjs/operators';
import { NetworkElementSearchResponseForUI } from '../model/network-element-search-response-for-ui';
import { NetworkElementForUI } from '../model/network-element-for-ui';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class NetworkElementService {

  private networkElementSearchUrl: string;
  private networkElementGetByIdUrl: string;
  private networkElementCreateUrl: string;
  private networkElementUpdateUrl: string;
  private uiDateFormat: string;
  private readonly backendNeDateFormat = "YYYY-MM-DDTHH:mm:ss";

  //declaring it private in constructor also creates the "hidden" global (class) private field http:
  constructor(private http: HttpClient) {
    //environment variable is available in all classes, and it has the content of the environment.xxx.ts file 
    //(i.e.: environment.ts when in dev mode, environment.prod.ts when in prod mod, or environment.[my_custon_environment].ts when app is started with --configuration=[my_custon_environment])
    this.networkElementSearchUrl = environment.backEnd.rootUrl + environment.backEnd.endpointUrls.networkElementSearch;
    this.networkElementGetByIdUrl = environment.backEnd.rootUrl + environment.backEnd.endpointUrls.networkElementGetById;
    this.networkElementCreateUrl = environment.backEnd.rootUrl + environment.backEnd.endpointUrls.networkElementCreate;
    this.networkElementUpdateUrl = environment.backEnd.rootUrl + environment.backEnd.endpointUrls.networkElementUpdate;
    this.uiDateFormat = environment.frontEnd.uiDateFormat;
  }

  search(request: NetworkElementSearchRequest): Observable<NetworkElementSearchResponseForUI> {
    var backendSearchRequest = {
      sortField: request.sort.field,
      sortOrder: request.sort.order == SortDirection.Asc ? "asc" : "desc",
      from: request.from,
      size: request.size
    };
    //the http.post function accepts a generics specification. The returned json will be marshalled to an instance of that generic:
    var responseObservable: Observable<NetworkElementSearchResponse> = this.http.post<NetworkElementSearchResponse>(this.networkElementSearchUrl,
      backendSearchRequest);

    /* to manipulate (transform), the *content* of the object returned by this Observable:
      - "import { map } from 'rxjs/operators';" see top part of this class file
      - first use ".pipe" to get the object, specifying as generics the type we want to return from this (NetworkElementSearchResponseForUI)
      - in .pipe we use ".map", specifying the 2 generics for input and return (NetworkElementSearchResponse and NetworkElementSearchResponseForUI)
      - from map we need to return what we want to have in the new Observable 
      - finally, the return of the call to pipe (with map) needs to be put in a new variable (the original Observable is not altered by pipe/map, a new Observable is created by this)
      here, we can use the same variable as the one for the original Observable, since the generic type didn't change 
    */
    var observableForUI: Observable<NetworkElementSearchResponseForUI> = responseObservable.pipe<NetworkElementSearchResponseForUI>(map<NetworkElementSearchResponse, NetworkElementSearchResponseForUI>(neSearchResponse => {
      var neSearchResponseForUI = new NetworkElementSearchResponseForUI();
      var nesForUI = new Array<NetworkElementForUI>();
      neSearchResponseForUI.results = nesForUI;
      neSearchResponseForUI.totalHits = neSearchResponse.totalHits;

      for (var i = 0; i < neSearchResponse.results.length; i++) {
        var ne = neSearchResponse.results[i];
        var neForUI = this.convertNetworkElement(ne);
        nesForUI[i] = neForUI;
      }
      return neSearchResponseForUI;
    }));

    return observableForUI;
  }

  getById(id: number): Observable<NetworkElement> {
    return this.http.get<NetworkElement>(this.networkElementGetByIdUrl, { params: new HttpParams().set("ID", id.toString()) });
  }

  getForUIById(id: number): Observable<NetworkElementForUI> {
    return this.http
      .get<NetworkElement>(this.networkElementGetByIdUrl, { params: new HttpParams().set("ID", id.toString()) })
      .pipe<NetworkElementForUI>(map<NetworkElement, NetworkElementForUI>(ne => {
        return this.convertNetworkElement(ne);
      }));
  }

  /*
  The REST endpoints for creating and updating a NE always return an HTTP response with empty body
  We should return the Observable for that (generically typed to an Object, which, in this case will always be an empty instace),
  so that the caller can deal with any errors that might have happend with the HTTP call:
  */
  persistNetworkElement(networkElement: NetworkElement): Observable<Object> {
    if (networkElement.ID == null) {
      return this.http.post(this.networkElementCreateUrl, networkElement);
    } else {
      return this.http.post(this.networkElementUpdateUrl, networkElement);
    }
  }

  persistNetworkElementForUI(networkElementForUI: NetworkElementForUI): Observable<Object> {
    var networkElement: NetworkElement = this.convertNetworkElementForUI(networkElementForUI);
    return this.persistNetworkElement(networkElement);
  }

  private convertNetworkElement(ne: NetworkElement): NetworkElementForUI {
    var neForUI = new NetworkElementForUI();
    neForUI.NAME = ne.NAME;
    neForUI.ID = ne.ID;
    neForUI.IP = ne.IP;
    neForUI.PRODUCT_FAMILY = ne.PRODUCT_FAMILY;
    neForUI.PRODUCT_NAME = ne.PRODUCT_NAME;
    neForUI.GROUP = ne.GROUP;
    neForUI.SLOTS = ne.SLOTS;
    neForUI.LAST_UPDATE_TIMESTAMP = ne.LAST_UPDATE_TIMESTAMP == null ? 
      null : 
      moment(ne.LAST_UPDATE_TIMESTAMP).format(this.uiDateFormat);
    return neForUI;
  }

  private convertNetworkElementForUI(neForUI: NetworkElementForUI): NetworkElement {
    var ne = new NetworkElement();
    ne.NAME = neForUI.NAME;
    ne.ID = neForUI.ID;
    ne.IP = neForUI.IP;
    ne.PRODUCT_FAMILY = neForUI.PRODUCT_FAMILY;
    ne.PRODUCT_NAME = neForUI.PRODUCT_NAME;
    ne.GROUP = neForUI.GROUP;
    ne.SLOTS = neForUI.SLOTS;
    ne.LAST_UPDATE_TIMESTAMP = neForUI.LAST_UPDATE_TIMESTAMP == null ? 
      null : 
      moment(neForUI.LAST_UPDATE_TIMESTAMP, this.uiDateFormat, true).format(this.backendNeDateFormat);
    return ne;
  }
}
