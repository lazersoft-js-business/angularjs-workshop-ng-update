import { FormControl } from '@angular/forms';

//a function can be exported as a stand-alone unit (just like a class)
//this is usefull here, as all we need to encapsulate the validation logic for a HTML template input is a function (no need to create a class for it)
export function ipFormValidator(control: FormControl) {

    //in TypeScript, the RegExp object can be initialized by passing a regex text, without any quotes, surrounded by "/" with optional regex flags at end (gm, here)
    const IP_REGEX:RegExp = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gm;

    var value = control.value;
    if (value == null || value == "") {
        // by returning this object (with the field required set to something not null), 
        // in the HTML template we will be able to check it's presence via "ipValidator.errors.required" (in *ngIf)
        return {
            required: true
        }
    }

    if(value.split(".").length != 4) {
        //more complex error objects can be returned
        //the below will be addressable in the HTML template as *ngIf="ip.errors.structure.threeDots"
        return {
            structure: {
                threeDots: true
            }
        }
    }

    if(!IP_REGEX.test(value)) {
        return {
            structure: {
                ipv4: true
            }
        }
    }

    //return null when the validation passed (no validation error message to return)
    return null;
}