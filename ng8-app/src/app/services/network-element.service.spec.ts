import { TestBed } from '@angular/core/testing';

import { NetworkElementService } from './network-element.service';

//added import for http stuff needed by service:
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

import { NetworkElement } from '../model/network-element';

// importing the dev environment:
import { environment } from 'src/environments/environment';

describe('NetworkElementService', () => {
  let httpClient: HttpClient;

  beforeEach(() => {
    //added import for http stuff needed by service:
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClient = TestBed.get(HttpClient);
  });

  //this was the default test that was present in the auto-generated file:
  it('should be created', () => {
    const service: NetworkElementService = TestBed.get(NetworkElementService);
    expect(service).toBeTruthy();
  });

  //this an actual test that was added to the auto-generated stub:
  it('should call the creation REST endpoint when no ID is specified for NetworkElement', () => {
    const service: NetworkElementService = TestBed.get(NetworkElementService);

    var httpClientPostSpy = spyOn(httpClient, "post");

    var networkElement = new NetworkElement();
    service.persist(networkElement);

    expect(httpClientPostSpy).toHaveBeenCalledWith(environment.backEnd.rootUrl + environment.backEnd.endpointUrls.networkElementCreate, networkElement);
  });

  //this an actual test that was added to the auto-generated stub:
  it('should call the update REST endpoint when an ID is specified for NetworkElement', () => {
    const service: NetworkElementService = TestBed.get(NetworkElementService);

    var httpClientPostSpy = spyOn(httpClient, "post");

    var networkElement = new NetworkElement();
    networkElement.ID = 1;
    service.persist(networkElement);

    expect(httpClientPostSpy).toHaveBeenCalledWith(environment.backEnd.rootUrl + environment.backEnd.endpointUrls.networkElementUpdate, networkElement);
  });
});
