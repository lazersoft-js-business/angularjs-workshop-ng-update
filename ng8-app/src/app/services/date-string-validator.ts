import { FormControl } from '@angular/forms';

import * as moment from 'moment';

//this top function is not the actual validator, but a "factory" function, that will return the actual validator function,
//parametrized with the desired date format, similar to standard validator "min": Validators.min(5)
export function dateStringValidator(dateFormat:string) {
    return function(control:FormControl) {
        if (control.value == null || control.value == "" || moment(control.value, dateFormat, true).isValid()) {
            //this validator accepts empty value (it's not required)
            return null;
          } else {
            return {
              /*
              Here, when returning a (non-null) error object, we'll not just set the property to (boolean) true.
              Instead, we'll set it to the actual desired date format.
              This way, in the HTML template, testing for errors on this can still use *ngIf like with the other validators:
              *ngIf="lastUpdateTimestamp.errors.dateFormat" will return true,
              but now, we can also display in the HTML template a validation error message than contains the actual desired date format {{lastUpdateTimestamp.errors.dateFormat}}
              */
              dateFormat: dateFormat
            }
          }
    }
}