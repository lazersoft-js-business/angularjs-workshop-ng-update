import { Component } from '@angular/core';
import { MainMenuEntry } from './model/main-menu-entry';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  originalMainMenuEntries:MainMenuEntry[] = [
    {label: "Network Elements List", route: "/network-element-list"}, 
    {label: "Create Network Element (using basic input)", route: "/network-element-edit"},
    {label: "Create Network Element (using Angular Reactive Forms)", route: "/network-element-edit-with-validation"}        
  ];
  extraMainMenuEntries:MainMenuEntry[] = [
    {label: "Debug", route: "/debug"}
  ];
}
