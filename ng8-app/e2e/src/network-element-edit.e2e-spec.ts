import { browser, logging } from 'protractor';
import { NetworkElementEditPage } from './network-element-edit.po';

describe('workspace-project App', () => {
  let page: NetworkElementEditPage;

  beforeEach(() => {
    page = new NetworkElementEditPage();
  });

  it('should display page in NE Creation mode', () => {
    page.navigateToNECreationMode();
    expect(page.getViewTitleText()).toEqual('Create Network Element');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
