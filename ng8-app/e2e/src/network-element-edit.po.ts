import { browser, by, element } from 'protractor';

// the ".po." in the name of this file referes to "Page Object"
export class NetworkElementEditPage {
  navigateToNECreationMode() {
    return browser.get(browser.baseUrl + '/network-element-edit') as Promise<any>;
  }

  // return of function can be skipped in function declaration:
  getViewTitleText() {
    return element(by.tagName('h2')).getText() as Promise<string>;
  }
}
