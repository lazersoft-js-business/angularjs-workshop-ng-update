var myModule = angular.module("myApp", []);

myModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

myModule.controller("myController", ["$http", "$scope", "$location", function($http, $scope, $location) {
	$scope.internalN1 = 1;
	$scope.internalN2 = 2;
	
	console.log("Is internalN1 === 1 ? : " + ($scope.internalN1 === 1));
	console.log("Is internalN2 === 2 ? : " + ($scope.internalN2 === 2));
	
	$scope.internalSum = $scope.internalN1 + $scope.internalN2;
	
	//using values provided by user:
	
	var locationSearch = $location.search();
	
	$scope.n1 = locationSearch.n1;
	console.log("Got n1: " + $scope.n1);
	console.log("Is n1 === 1 ? : " + ($scope.n1 === 1));
	
	$scope.n2 = locationSearch.n2;
	console.log("Got n2: " + $scope.n2);
	console.log("Is n2 === 2 ? : " + ($scope.n2 === 2));
	
	$scope.sum = $scope.n1 + $scope.n2;	
}]);