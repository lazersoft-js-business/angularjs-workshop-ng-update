var neModule = angular.module("neApp", []);

neModule.controller("neController", ["$http", "$scope", function($http, $scope) {
	
	var ctrl = this;
	
	$scope.productFamilies = ["ip10", "ip20"];
	
	$scope.productNamesByFamily = [];
	$scope.productNamesByFamily["ip10"] = ["ip10c", "ip10e", "ip10g"];
	$scope.productNamesByFamily["ip20"] = ["ip20f", "ip20e", "ip20s"];
	
	$scope.groupsByFamily = [];
	$scope.groupsByFamily["ip10"] = ["1+0", "HSB", "MR"];
	$scope.groupsByFamily["ip20"] = ["1+0", "ABC", "RPG"];
	
	$scope.networkElement = {};
	$scope.networkElement.GROUP = "1+0";
	
	ctrl.performSubmit = function() {		
		$http.post("http://localhost:8765/ang/back-end/network-element/create", $scope.networkElement).then(function(httpResponse) {
			$scope.submitResult = "OK, Id is: " + httpResponse.data.id;
		}, function(httpErrorResponse) {
			$scope.submitResult = "Error: httpErrorResponse.status=" + httpErrorResponse.status + 
				" httpErrorResponse.statusText=" + httpErrorResponse.statusText;
			$scope.submitErrorDetails = httpErrorResponse.data
		});
	}
}]);