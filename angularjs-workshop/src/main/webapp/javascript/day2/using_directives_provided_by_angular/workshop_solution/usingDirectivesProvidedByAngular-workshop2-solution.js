var myModule = angular.module("myApp", []);

myModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

myModule.controller("myController", ["$http", "$scope", "$location", function($http, $scope, $location) {
	
	$scope.lapsDurations = [];
	$scope.stateDescription = "Stopped";
	
	var startTimeMs;
	var lastLapStartTimeMs;
	
	$scope.start = function() {
		startTimeMs = currentTimeMillis();
		lastLapStartTimeMs = startTimeMs;
		$scope.stateDescription = "Started";
	};
	
	$scope.stop = function() {
		startTimeMs = undefined;
		lastLapStartTimeMs = undefined;
		$scope.lapsDurations = [];
		$scope.stateDescription = "Stopped";
	};
	
	$scope.lap = function() {
		var current = currentTimeMillis();
		$scope.lapsDurations.push(current - lastLapStartTimeMs);
		lastLapStartTimeMs = current;
	};
	
	//two-way-binding also works for functions (returns):
	$scope.isStarted = function() {
		return startTimeMs !== undefined;
	};
	
	function currentTimeMillis() {
		return new Date().getTime();
	}
}]);