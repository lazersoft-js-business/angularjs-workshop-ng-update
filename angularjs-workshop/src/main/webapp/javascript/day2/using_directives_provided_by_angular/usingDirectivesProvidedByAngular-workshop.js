var myModule = angular.module("myApp", []);

myModule.controller("myController", ["$http", "$scope", function($http, $scope) {
	
	$scope.hiddenButtons = [];
	
	$scope.hideButton = function(buttonIndex) {
		$scope.hiddenButtons[buttonIndex] = true;
	};
	
	$scope.showAll = function() {
		for(var i = 0; i < $scope.hiddenButtons.length; i++) {
			$scope.hiddenButtons[i] = false;
		}
	};
}]);