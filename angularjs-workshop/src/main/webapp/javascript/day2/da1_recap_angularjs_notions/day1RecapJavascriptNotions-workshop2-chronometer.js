var myModule = angular.module("myApp", []);

myModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

myModule.controller("myController", ["$scope", function($scope) {
	
	//TODO
	
	function currentTimeMillis() {
		return new Date().getTime();
	}
}]);