var myModule = angular.module("myApp", []);

myModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

//a directive must, at the minumum, have a name (millisecondsFormat), and, as second argument,
//a function that returns an object containing some specific fields, used for directive configuration:
myModule.directive("millisecondsFormat", function() {
	return {
		//the "scope" field must contain an object, who's fields represent the attributes that this directive
		//supports (for parametrization of directive behavior):
		scope: {
			//the value of each attribute must be one from a fixed set
			//this specifies what kind of value the attribute supports: "=" means another variable can be passsed as value, 
			//"@" means that a simple text primitive can be passed as value, etc
			millisecondsValue: '=',
			scalingFactor: "@",
			timeUnitLabel: "@"
	    },
	    //this specifies where the UI template (in HTML format) for this directive can be found
		templateUrl: "./millisecondsFormatView.html"
	};
});

myModule.controller("myController", ["$scope", function($scope) {
	$scope.lapsDurations = [];
	$scope.stateDescription = "Stopped";
	
	var startTimeMs;
	var lastLapStartTimeMs;
	
	$scope.start = function() {
		startTimeMs = currentTimeMillis();
		lastLapStartTimeMs = startTimeMs;
		$scope.stateDescription = "Started";
	};
	
	$scope.stop = function() {
		startTimeMs = undefined;
		lastLapStartTimeMs = undefined;
		$scope.lapsDurations = [];
		$scope.stateDescription = "Stopped";
	};
	
	$scope.lap = function() {
		var current = currentTimeMillis();
		$scope.lapsDurations.push(current - lastLapStartTimeMs);
		lastLapStartTimeMs = current;
	};
	
	function currentTimeMillis() {
		return new Date().getTime();
	}
}]);