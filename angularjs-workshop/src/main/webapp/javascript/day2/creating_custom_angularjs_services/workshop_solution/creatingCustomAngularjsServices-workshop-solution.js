var myModule = angular.module("myApp", []);

myModule.service("appConfigurationService", function() {
	var appConfigurationService = {
			productFamilyInUpperCase: false,
			productNameInUpperCase: true
	};
	
	return appConfigurationService;
});

myModule.service("networkElementFormatterService", ["appConfigurationService", function(appConfigurationService) {
	return new NetworkElementFormatterService(appConfigurationService.productFamilyInUpperCase, 
			appConfigurationService.productNameInUpperCase111);
}]);

myModule.controller("myController", ["$http", "$scope", "networkElementFormatterService", "appConfigurationService", 
	function($http, $scope, networkElementFormatterService, appConfigurationService) {
	
	$scope.appConfigurationService = appConfigurationService;
	
	$http.get("http://localhost:8765/ang/back-end/network-element/all").then(function(httpResponse) {
		var formattedNetworkElements = networkElementFormatterService.format(httpResponse.data);
		$scope.networkElements = formattedNetworkElements;
	});
}]);