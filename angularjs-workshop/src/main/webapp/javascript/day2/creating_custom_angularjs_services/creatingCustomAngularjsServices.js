var myModule = angular.module("myApp", []);

myModule.service("networkElementFormatterService", function() {
	var productFamilyInUpperCase = true;
	var productNameInUpperCase = false
	var networkElementFormatterService = new NetworkElementFormatterService(productFamilyInUpperCase, productNameInUpperCase);
	return networkElementFormatterService;
});

myModule.controller("myController", ["$http", "$scope", "networkElementFormatterService", function($http, $scope, networkElementFormatterService) {
	
	$http.get("http://localhost:8765/ang/back-end/network-element/all").then(function(httpResponse) {
		var formattedNetworkElements = networkElementFormatterService.format(httpResponse.data);
		$scope.networkElements = formattedNetworkElements;
	});
}]);