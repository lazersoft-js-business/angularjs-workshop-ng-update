/*
 * @constructor
 */
function NetworkElementFormatterService(productFamilyInUpperCase, productNameInUpperCase) {
	
	var self = this;
	
	self.productFamilyInUpperCase = productFamilyInUpperCase;
	self.productNameInUpperCase = productNameInUpperCase;
	
	/*
	 * @public
	 */
	self.format = function(netoworkElements) {
		var formattedNetoworkElements = [];
		for(var i = 0; i < netoworkElements.length; i++) {
			var networkElementClone = cloneNetworkElement(netoworkElements[i]);
			if(self.productFamilyInUpperCase) {
				networkElementClone.PRODUCT_FAMILY = networkElementClone.PRODUCT_FAMILY.toUpperCase();
			}
			if(self.productNameInUpperCase) {
				networkElementClone.PRODUCT_NAME = networkElementClone.PRODUCT_NAME.toUpperCase();
			}
			formattedNetoworkElements.push(networkElementClone);
		}
		return formattedNetoworkElements;
	};
	
	/*
	 * @private
	 */
	function cloneNetworkElement(originalNetworkElement) {
		var networkElementClone = {};
		networkElementClone.ID = originalNetworkElement.ID;
		networkElementClone.NAME = originalNetworkElement.NAME;
		networkElementClone.PRODUCT_FAMILY = originalNetworkElement.PRODUCT_FAMILY;
		networkElementClone.PRODUCT_NAME = originalNetworkElement.PRODUCT_NAME;
		networkElementClone.GROUP = originalNetworkElement.GROUP;
		networkElementClone.SLOTS = originalNetworkElement.SLOTS;
		networkElementClone.LAST_UPDATE_TIMESTAMP = originalNetworkElement.LAST_UPDATE_TIMESTAMP;
		networkElementClone.INSTALLATION_DATE = originalNetworkElement.INSTALLATION_DATE;
		return networkElementClone;
	};	
}