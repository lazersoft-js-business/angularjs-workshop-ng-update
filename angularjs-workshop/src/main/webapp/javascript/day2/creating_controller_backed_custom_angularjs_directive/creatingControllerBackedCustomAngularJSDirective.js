var myModule = angular.module("myApp", []);

myModule.config( [ '$locationProvider', function( $locationProvider ) {
   $locationProvider.html5Mode( true );
}]);

myModule.directive("textTrim", function() {
	return {
		scope: {
			text: '=',
			maxChars: "="
	    },
	    templateUrl: "./textTrim.html",
	    controller: ['$scope', function textTrimController($scope) {
	       
	    	$scope.textToDisplay = $scope.text;
	    	if($scope.text.length > $scope.maxChars) {
	    		$scope.textToDisplay = $scope.text.substring(0, $scope.maxChars) + "...";
	    	}
	    }]
	};
});

myModule.controller("myController", ["$scope", function($scope) {
	$scope.networkElementsNames = [
		"My IP10",
		"My IP20g",
		"My aaaa AAAA IP20G",
		"Some IP10F",
		"aaaaaa name goes here aaaaa"
	];
}]);