appModule.controller("networkElementEditController", ["$http", "$scope", "$location", function($http, $scope, $location) {
	
	var ctrl = this;
	
	var selectedId = $location.search().ID;
	
	$scope.operationTypeDisplay = selectedId === undefined ? "Create" : "Edit";
	$scope.networkElement;
	
	$scope.productFamilies = ["ip10", "ip20"];
	
	$scope.productNamesByFamily = [];
	$scope.productNamesByFamily["ip10"] = ["ip10c", "ip10e", "ip10g"];
	$scope.productNamesByFamily["ip20"] = ["ip20a", "ip20f", "ip20e", "ip20s", "ip20c", "ip20n"];
	
	$scope.groupsByFamily = [];
	$scope.groupsByFamily["ip10"] = ["1+0", "HSB", "MR"];
	$scope.groupsByFamily["ip20"] = ["1+0", "ABC", "RPG"];
	
	ctrl.goToNetworkElementList = function() {
		$location.path("/network-element-list");
	};
	
	ctrl.persistNetworkElement = function() {
		if(selectedId === undefined) {
			$http.post("http://localhost:8765/ang/back-end/network-element/create", $scope.networkElement).then(ctrl.goToNetworkElementList);
		} else {
			$http.post("http://localhost:8765/ang/back-end/network-element/update", $scope.networkElement).then(ctrl.goToNetworkElementList);
		}
	};
	
	function init() {
		if(selectedId !== undefined) {
			$http({
				method: "GET",
			    url: "http://localhost:8765/ang/back-end/network-element/get-by-id", 
			    params: {ID: selectedId}
			}).then(function(httpResponse) {
				$scope.networkElement = httpResponse.data;
			});
		}
	}
	
	init();
}]);