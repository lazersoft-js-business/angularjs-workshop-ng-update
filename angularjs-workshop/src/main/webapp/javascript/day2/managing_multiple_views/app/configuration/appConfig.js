//init our application's module. Controllers, directives and other Angular components need to be added to this module when declared later on
var appModule = angular.module("app", ["ngRoute"]);

//configure our application's module: specify what URL routes will load what controllers
appModule.config([
	
	"$routeProvider",
	
	function($routeProvider) {		
		var networkElementListRouteConfig = {
			templateUrl: "controllers/networkElementList/networkElementListView.html",
			controller: "networkElementListController",
			controllerAs: "ctrl"
		};
		
		var networkElementEditRouteConfig = {
			templateUrl: "controllers/networkElementEdit/networkElementEditView.html",
			controller: "networkElementEditController",
			controllerAs: "ctrl"
		};
		
		$routeProvider.when("/network-element-list", networkElementListRouteConfig);
		$routeProvider.when("/network-element-edit", networkElementEditRouteConfig);
	}
]);
