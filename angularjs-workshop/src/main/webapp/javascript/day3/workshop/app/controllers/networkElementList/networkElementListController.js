appModule.controller("networkElementListController", ["$http", "$scope", "$location", function($http, $scope, $location) {
	var ctrl = this;
	
	$scope.columns = [
		{field: "ID", 						displayLabel: "Id"},
		{field: "NAME", 					displayLabel: "Name"},
		{field: "PRODUCT_FAMILY", 			displayLabel: "Product Family"},
		{field: "PRODUCT_NAME", 			displayLabel: "Product Name"},
		{field: "IP", 						displayLabel: "IP"},
		{field: "SLOTS", 					displayLabel: "Slots"},
		{field: "GROUP", 					displayLabel: "Group Configuration"},
		{field: "LAST_UPDATE_TIMESTAMP", 	displayLabel: "Last Updated"}
	];
	
	$scope.goToNetworkElementEdit = function(networkElementId) {
		if(networkElementId === undefined) {
			$location.path("/network-element-edit");
		} else {
			$location.path("/network-element-edit" + "/" + networkElementId);
		}
	};
	
	$scope.networkElementSearchResponse;
	
	$scope.loadDataForCurrentPage = function() {
		var neSearchRequest = {
			from: 0,
			size: 100
		};
		$http.post("http://localhost:8765/ang/back-end/network-element/search", neSearchRequest).then(function(httpResponse) {
			$scope.networkElementSearchResponse = httpResponse.data;
		});
	};
	
	function init() {
		$scope.loadDataForCurrentPage();
	}
	
	init();
}]);