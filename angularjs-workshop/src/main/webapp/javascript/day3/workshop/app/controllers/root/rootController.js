appModule.controller("rootController", ["$scope", "$location", function($scope, $location) {
	var ctrl = this;
	
	ctrl.locations = [
		{
			path: "/network-element-list",
			display: "Network Elements List"
		},
		{
			path: "/network-element-edit",
			display: "Create Network Element"
		}
	];
}]);