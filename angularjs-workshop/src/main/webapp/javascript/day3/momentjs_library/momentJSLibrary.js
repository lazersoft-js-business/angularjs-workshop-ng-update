
function main() {
	//for date strings in well-known formats, momentjs will auto-parse them without needing a parse format string:
	var momentObject1 = moment("2018-11-22T15:30");
	console.log("momentObject1=" + momentObject1);
	console.log("momentObject1.format(\"YYYY/MM/DD HH:mm\")=" + momentObject1.format("YYYY/MM/DD HH:mm"));
	
	//for date strings in strange formats, momentjs can be configured with a parse format string:
	// !Please note! the format tokens are not the same as in Java (i.e. momentjs uses Y for year, Java uses y. Similar for D and d, respectively, for day):
	var momentObject2 = moment("2018|11|22---15 30", "YYYY|MM|DD---HH mm");
	console.log("momentObject2.format(\"YYYY/MM/DD HH:mm\")=" + momentObject2.format("YYYY/MM/DD HH:mm"));
	
	//has convenience method for checking if a date string is valid (according to the given format):
	console.log("moment(\"2018/13/32 15:30\", \"YYYY/MM/DD HH:mm\").isValid()", moment("2018/13/32 15:30", "YYYY/MM/DD HH:mm").isValid()); //returns false
	
	//the default behavior for validity checking is pretty lenient: it will accept a 1 digit hour, even thought the formt specifies 2 digits:
	console.log("moment(\"2018/12/31 5:30\", \"YYYY/MM/DD HH:mm\").isValid()", moment("2018/12/31 5:30", "YYYY/MM/DD HH:mm").isValid());// returns true
	
	//validity checking can be made strinct, by passing a 3rd value as argument: true
	console.log("moment(\"2018/12/31 5:30\", \"YYYY/MM/DD HH:mm\", true).isValid()", moment("2018/12/31 5:30", "YYYY/MM/DD HH:mm", true).isValid());// returns false
	
	var momentA = moment("2018-11-22T09:17:00");
	var momentB = moment("2018-11-22T13:47:00");
	var duration = moment.duration(momentB.diff(momentA));
	console.log("duration=" + duration);
	console.log("duration.humanize()=" + duration.humanize());
	console.log("duration.humanize(true)=" + duration.humanize(true));
};