var myModule = angular.module("myApp", []);

myModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

myModule.controller("myController", ["$scope", function($scope) {
	var ctrl = this;
	
	ctrl.someNumber;
	
	$scope.$watch("ctrl.someNumber", function(newValue, oldValue) {
		if(newValue % 4 ===0) {
			console.log("New value: " + newValue + " is divisible by 4");
		}
	});
}]);