appModule.controller("networkElementListController", ["$http", "$scope", "$location", function($http, $scope, $location) {
	var ctrl = this;
	
	$scope.columns = [
		{field: "ID", 						displayLabel: "Id"},
		{field: "NAME", 					displayLabel: "Name"},
		{field: "PRODUCT_FAMILY", 			displayLabel: "Product Family"},
		{field: "PRODUCT_NAME", 			displayLabel: "Product Name"},
		{field: "IP", 						displayLabel: "IP"},
		{field: "SLOTS", 					displayLabel: "Slots"},
		{field: "GROUP", 					displayLabel: "Group Configuration"},
		{field: "LAST_UPDATE_TIMESTAMP", 	displayLabel: "Last Updated"}
	];
	
	$scope.sortColumn = {
			field: "ID",
			order: "asc"
	}
	
	$scope.goToNetworkElementEdit = function(networkElementId) {
		if(networkElementId === undefined) {
			$location.path("/network-element-edit");
		} else {
			$location.path("/network-element-edit" + "/" + networkElementId);
		}
	};
	
	$scope.pageSize = 5;
	//page numbering is 1-based (not 0-based). First page is 1:
	$scope.currentPage = 1;
	
	//good:
	ctrl.networkElementSearchResponse;
	
	//bad!:
//	this.networkElementSearchResponse;
	
	ctrl.loadDataForCurrentPage = function() {
		var neSearchRequest = {
			from: ($scope.currentPage - 1) * $scope.pageSize,
			size: $scope.pageSize,
			sortField: $scope.sortColumn.field,
			sortOrder: $scope.sortColumn.order
		};
		$http.post("http://localhost:8765/ang/back-end/network-element/search", neSearchRequest).then(function(httpResponse) {
			for(var i = 0; i < httpResponse.data.results.length; i++) {
				var ne = httpResponse.data.results[i];
				
				//moment js can auto-detect-parse a date string, without any format information:
				ne.LAST_UPDATE_TIMESTAMP = moment(ne.LAST_UPDATE_TIMESTAMP).format("YYYY/MM/DD HH:mm:ss");
				
				//if we wanted to manually set a particular parse pattern, we could do it like this:
				//WARNING! the date pattern tokens used by momentjs are not the same as those used by Java (example: for 4-digit year: momentjs: YYYY, Java: yyyy) 
//				ne.LAST_UPDATE_TIMESTAMP = moment(ne.LAST_UPDATE_TIMESTAMP, "YYYY-MM-DDTHH:mm:ss").format("YYYY/MM/DD HH:mm");
			}
			
			//good:
			ctrl.networkElementSearchResponse = httpResponse.data;
			
			//bad!:
//			this.networkElementSearchResponse = httpResponse.data;
		});
	};
	
	$scope.updateSortColumn = function(column) {
		if($scope.sortColumn.field === column.field) {
			//when the user has clicked again on the currently sorting column, change its direction:
			$scope.sortColumn.order = $scope.sortColumn.order === "asc" ? "desc" : "asc";
		} else {
			//we have a new sort column, set it to asc:
			$scope.sortColumn.field = column.field;
			$scope.sortColumn.order = "asc";			
		}
		
		$scope.currentPage = 1;
		ctrl.loadDataForCurrentPage();
	};
	
	function starWatchingPageSize() {
		$scope.$watch("pageSize", function() {
			$scope.currentPage = 1;
			ctrl.loadDataForCurrentPage();			
		});
	};
	
	function init() {
		starWatchingPageSize();
		
		//good:
		ctrl.loadDataForCurrentPage();
		
		//bad!:
//		this.loadDataForCurrentPage();
	};
	
	init();
}]);