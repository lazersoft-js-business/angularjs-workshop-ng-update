var dataStructuresAndLogicalOperatorsInJsModule = angular.module("dataStructuresAndLogicalOperatorsInJsApp", []);

dataStructuresAndLogicalOperatorsInJsModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

dataStructuresAndLogicalOperatorsInJsModule.controller("dataStructuresAndLogicalOperatorsInJsController", ["$http", "$scope", "$location", function($http, $scope, $location) {
	
	//get value of URL query param ID, and store it here:
	var selectedId;
	
	//this variable will contain an empty string if selectedId is OK, or a generic error message otherwise ("no network element id specified in URL")
	$scope.errorMessage = "";
	
	//validate selectedId is OK, and update $scope.errorMessage accordingly
	
	//if selectedId is ok, use $http to load the corresponding network element, into $scope.networkElement
}]);