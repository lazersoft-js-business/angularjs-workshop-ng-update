var dataStructuresAndLogicalOperatorsInJsModule = angular.module("dataStructuresAndLogicalOperatorsInJsApp", []);

dataStructuresAndLogicalOperatorsInJsModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

dataStructuresAndLogicalOperatorsInJsModule.controller("dataStructuresAndLogicalOperatorsInJsController", ["$http", "$scope", "$location", function($http, $scope, $location) {
	
	var locationSearch = $location.search();
	var selectedId = locationSearch.ID;
	
	$scope.errorMessage = "";
	
	if(selectedId === undefined || selectedId === "") {
		$scope.errorMessage = "no network element id specified in URL";
	} else {
		$http.get("http://localhost:8765/ang/back-end/network-element/get-by-id?ID=" + selectedId).then(function(httpResponse) {
			$scope.networkElement = httpResponse.data;
		});
		
		//a cleaner way to do an HTTP GET, and specify URL parameters is:
		$http({
			method: "GET",
		    url: "http://localhost:8765/ang/back-end/network-element/get-by-id", 
		    params: {ID: selectedId}
		}).then(function(httpResponse) {
			$scope.networkElement = httpResponse.data;
		});
	}
}]);