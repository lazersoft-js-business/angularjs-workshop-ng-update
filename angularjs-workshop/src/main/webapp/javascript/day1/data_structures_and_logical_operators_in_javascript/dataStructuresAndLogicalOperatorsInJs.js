
function main() {
//	showcaseDataStructures();
	showcaseLogicalOperators();
};

function showcaseDataStructures() {
	var simpleArray1 = ["apples", "banans", "pears"];
	console.log("simpleArray1: " + simpleArray1);
	
	console.log("____________________");
	
	var simpleArray2 = [];
	simpleArray2[0] = "apples";
	simpleArray2[1] = "banans";
	simpleArray2[2] = "pears";
	console.log("simpleArray2: " + simpleArray2);
	
	console.log("____________________");
	
	var stringKeyMap = [];
	stringKeyMap["red"] = "apple";
	stringKeyMap["yellow"] = "banana";
	console.log("stringKeyMap[\"red\"]=" + stringKeyMap["red"]);
	console.log("stringKeyMap[\"yellow\"]=" + stringKeyMap["yellow"]);
	
	console.log("____________________");
	
	var stack = [];
	stack.push("apple");
	stack.push("banana");
	stack.push("orange");
	console.log("stack: " + stack);
	console.log("stack.length=" + stack.length);
	console.log("stack.pop()=" + stack.pop());
	console.log("stack: " + stack);
	console.log("stack.length=" + stack.length);
	
	console.log("____________________");
	
	var queue = [];
	queue.push("apple");
	queue.push("banana");
	queue.push("orange");
	console.log("queue: " + queue);
	console.log("queue.length=" + queue.length);
	console.log("queue.shift()=" + queue.shift());
	console.log("queue: " + queue);
	console.log("queue.length=" + queue.length);
	
	console.log("____________________");
};

function showcaseLogicalOperators() {
	var simpleArray1 = ["apples", "banans", "pears"];
	console.log("simpleArray1: " + simpleArray1);
	
	console.log("____________________");
	
	for(var i = 0; i < simpleArray1.length; i++) {
		var elem = simpleArray1[i];
		console.log("elem at position " + i + "=" + elem);
	}
	
	console.log("____________________");
	
	var nonExistngElement = simpleArray1[100];
	console.log("nonExistngElement=" + nonExistngElement);
	console.log("nonExistngElement === undefined " + (nonExistngElement === undefined));
	console.log("nonExistngElement == null " + (nonExistngElement == null));
	
	console.log("____________________");
	
	var intVar = 22;
	var stringVar = "22";
	
	console.log("intVar == 22 " + (intVar == 22));
	console.log("intVar === 22 " + (intVar === 22));
	
	console.log("stringVar == \"22\" " + (stringVar == "22"));
	console.log("stringVar === \"22\" " + (stringVar === "22"));	
	
	console.log("stringVar == intVar " + (stringVar == intVar)); // <- this returns true !!
	console.log("stringVar === intVar " + (stringVar === intVar));	
//	
//	/*
//	 * Always use triple equals (===), when comparing 2 variables. This ensures that not only do we check if the values are "similar", 
//	 * but also that both variables are of the same type
//	 */
//	
//	console.log("____________________");
//	
//	console.log("\"1\" == 1 " + ("1" == 1));
//	console.log("\"1\" === 1 " + ("1" === 1));
//	
//	console.log("____________________");
//	
//	/*
//	 * Always leave empty variable to their default value of undefined
//	 * Do not initialize them to null 
//	 */
//	
//	var noValueVariable;
//	var nullVariable = null;
//	console.log("noValueVariable=" + noValueVariable);
//	console.log("nullVariable=" + nullVariable);
//	console.log("noValueVariable === undefined " + (noValueVariable === undefined));
//	console.log("noValueVariable === null " + (noValueVariable === null)); // <- this returns false !
//	console.log("nullVariable === undefined " + (nullVariable === undefined)); // <- this returns false !
//	console.log("nullVariable === null " + (nullVariable === null));
};