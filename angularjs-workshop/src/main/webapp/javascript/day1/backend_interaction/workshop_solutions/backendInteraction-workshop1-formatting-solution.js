var backendInteractionModule = angular.module("backendInteractionApp", []);


/**
 * This is similar to how Spring Framework's beans work:
 * 
 * - the element to be injected is identified by a name. In this case we have the 2 string "$http", "$scope"
 * - when declaring the controller, the 2nd variable passed will not be just a function with no arguments (like we did before, for simpler controllers), but instead
 * will be an array, containing the variable names of all the injected elements, and, as a last element, the controller function, which now has those variables as arguments
 * - in this way, an existing service can be injected in a controller, and made available to its function*   
 * 
 */
backendInteractionModule.controller("backendInteractionController", ["$http", "$scope", function($http, $scope) {
	$http.get("http://localhost:8765/ang/back-end/network-element/all").then(formatNetworkElements);
	
	function formatNetworkElements(httpResponse) {
//		for(var i = 0; i < httpResponse.data.length; i++) {
//			var networkElement = httpResponse.data[i];
//			networkElement.PRODUCT_NAME = networkElement.PRODUCT_NAME.toUpperCase();
//			networkElement.PRODUCT_FAMILY = networkElement.PRODUCT_FAMILY.toUpperCase();
//		}
		
		$scope.networkElements = httpResponse.data;
	};
}]);