var backendInteractionModule = angular.module("backendInteractionApp", []);

backendInteractionModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

backendInteractionModule.controller("backendInteractionController", ["$http", "$scope", "$location", function($http, $scope, $location) {
	
	var locationSearch = $location.search();
	var selectedProductFamily = locationSearch.PRODUCT_FAMILY;
	
	//to check if a variable is empty (null or undefined) in JS, you can just check if it's false:
	if(selectedProductFamily === undefined) {
		$http.get("http://localhost:8765/ang/back-end/network-element/all").then(processHttpResponse);
	} else {
		var neSearchRequest = {
				"searchCriteria": [
				    {
				      "fieldName": "PRODUCT_FAMILY",
				      "fieldValue": selectedProductFamily
				    }
				  ]
		};
		$http.post("http://localhost:8765/ang/back-end/network-element/search", neSearchRequest).then(processHttpResponse);
	}
	
	function processHttpResponse(httpResponse) {
		//depending on the REST endpoint used, the response might have different formats:
		if(!selectedProductFamily) {
			$scope.networkElements = httpResponse.data;
		} else {
			$scope.networkElements = httpResponse.data.results;
		}
	};
}]);