var backendInteractionModule = angular.module("backendInteractionApp", []);

backendInteractionModule.config( [ '$locationProvider', function( $locationProvider ) {
   // In order to get the query string from the
   // $location object, it must be in HTML5 mode.
   $locationProvider.html5Mode( true );
}]);

backendInteractionModule.controller("backendInteractionController", ["$http", "$scope", /* inject $location service*/ function($http, $scope, $location /* declare variable for $location service */) {
	
	//get value of URL query param named "PRODUCT_FAMILY", using $location service, and store it in the "selectedProductFamily" variable:
	var selectedProductFamily;
	
	//if the URL query param *is not* present, get all the network elements (like before)
	
	//if the URL query param *is* present, call the back-end service that allows to search through network elements, and pass it the correct search query object:
	var neSearchRequest = {
		"searchCriteria": [
		    {
		      "fieldName": "PRODUCT_FAMILY",
		      "fieldValue": selectedProductFamily
		    }
		  ]
	};
	$http.post("http://localhost:8765/ang/back-end/network-element/search", neSearchRequest).then(processHttpResponse);
		
	function processHttpResponse(httpResponse) {
		/* expose returned network elements to the UI (in the $scope.networkElements array variable) */
		/* depending on what back-end service was called, the format of the results may differ */
	};
}]);