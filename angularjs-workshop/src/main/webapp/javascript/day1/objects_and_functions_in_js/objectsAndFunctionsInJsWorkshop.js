function main() {
	var httpService = new HttpService();
	var businessLogicService = new OrangeService("package");
	var appService = new AppService(httpService, businessLogicService);
	appService.runApp();
};

function HttpService() {
	
	this.getListOfItems = function(itemType, responseCallbackFunction) {
		var dummyListOfItems = [itemType + "1", itemType + "2", itemType + "3"];
		responseCallbackFunction(dummyListOfItems);
	}
	
};

function OrangeService(orangeProcessingData) {
	
	this.processingData = orangeProcessingData;
	
	this.processServiceResponse = function(listOfOranges) {
		console.log("{" + this.processingData +": " + listOfOranges + "}");
	}
};

function AppService(httpService, businessLogicService) {
	
	//TODO
	//call the httpService to get a list of itemType="orange", 
	//and use the "processServiceResponse" function from an OrangeService instance as callback, that will process the response received by the httpService
};