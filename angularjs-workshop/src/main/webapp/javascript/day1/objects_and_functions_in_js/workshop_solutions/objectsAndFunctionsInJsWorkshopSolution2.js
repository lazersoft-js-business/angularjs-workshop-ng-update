function main() {
	var httpService = new HttpService();
	var businessLogicService = new OrangeService("package");
	var appService = new AppService(httpService, businessLogicService);
	appService.runApp();
};

function HttpService() {
	
	this.getListOfItems = function(itemType, responseCallbackFunction) {
		var dummyListOfItems = [itemType + "1", itemType + "2", itemType + "3"];
		responseCallbackFunction(dummyListOfItems);
	}
	
};

function OrangeService(orangeProcessingData) {
	
	this.processingData = orangeProcessingData;
	
	this.processServiceResponse = function(listOfOranges) {
		console.log("{" + this.processingData +": " + listOfOranges + "}");
	}
};

function AppService(httpService, businessLogicService) {
	
	this.internalHttpService = httpService;
	this.internalBusinessLogicService = businessLogicService;
	
	this.runApp = function() {
		console.log("Solution 2");
		//solution 2:
		var self = this;
		this.internalHttpService.getListOfItems("orange", function(orangeProcessingData) {
			self.internalBusinessLogicService.processServiceResponse(orangeProcessingData);	
		});
	};
};