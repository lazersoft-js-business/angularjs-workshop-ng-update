function main() {
	var httpService = new HttpService();
	var businessLogicService = new OrangeService("package");
	var appService = new AppService(httpService, businessLogicService);
	appService.runApp();
};

function HttpService() {
	
	this.getListOfItems = function(itemType, servObj) {
		var dummyListOfItems = [itemType + "1", itemType + "2", itemType + "3"];
		servObj.responseCallbackFunction(dummyListOfItems);
	}
	
};

function OrangeService(orangeProcessingData) {
	
	this.processingData = orangeProcessingData;
	
	//solution 1:
	var self = this;
	
	this.processServiceResponse = function(listOfOranges) {
		//console.log("{" + this.processingData +": " + listOfOranges + "}");
		
		//solution 1:
		console.log("{" + self.processingData +": " + listOfOranges + "}");
	}
};

function AppService(httpService, businessLogicService) {
	
	this.internalHttpService = httpService;
	this.internalBusinessLogicService = businessLogicService;
	
	this.runApp = function() {
		//solution 1:
		this.internalHttpService.getListOfItems("orange", this.internalBusinessLogicService);	
	};
};