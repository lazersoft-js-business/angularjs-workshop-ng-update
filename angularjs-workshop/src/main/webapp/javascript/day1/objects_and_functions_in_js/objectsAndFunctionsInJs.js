
function main() {
	
	//simple way to create a new (empty object). Fields and functions are added afterwards, on the instance
	var myObject = {};	
	//add the "echo" function to the above-created object, by sotring it in the objects "myEcho" variable:
	myObject.myEcho = echo;
	
	//now we can call the original "echo" function, by using its variable, "myEcho", from "myObject":  
	var myEchoResult = myObject.myEcho("myEcho call");
	console.log("myEchoResult=" + myEchoResult);
	
	
	//// creating a parametrized object. A certain field inside the object is customized when the object instance is created
	//this can be though as somewhat similar to having a Java class, with a constructor (that has one argument):
	function myOtherObjectConstructor(textToEcho) {
		//inside a function, we can use the "this" keyword to declare fields and functions that will be available to new instances of the function
		//"this" needs to be used in conjunction with "new" (see below), to create a new instance of the function
		
		//create new instance variable inside the "myOtherObjectConstructor" function:
		this.textToEcho = textToEcho;
		
		var self = this;
		
		//create a new instance function, inside the "myOtherObjectConstructor" function:
		this.myEcho = function() {
			//console.log(">this< is now: " + self.constructor.name);
			return echo(self.textToEcho);
			
			//return echo(self.textToEcho);
		}
	};
	
	//when using the "new" keyword to call a function, that function acts as a constructor, creating a new instance of itself (a new object, which contains the function content):
	var myOtherObject = new myOtherObjectConstructor("myEcho other call");	
	//like with the previous object, we call a function that was defined on this object instance (myEcho):
	var myEchoOtherResult = myOtherObject.myEcho();
	console.log("myEchoOtherResult=" + myEchoOtherResult);
	
	
	//// in js, a function can always be allocated to a variable, even if that function originally belonged to an object instance:
	//here, we take the function "myEcho", from the object instance "myOtherObject", and we place it in a new variable "extractedEchoFunction"
	//the "extractedEchoFunction" does not belong to the "myOtherObject" instance
	var extractedEchoFunction = myOtherObject.myEcho;	
	//now we call the function, using this new variable:
	var extractedEchoFunctionResult = extractedEchoFunction();
	//the result is not the same as it was when we called this exact same function from the object instance
	//"this" is lost here. The function no longer belongs to an object instance, so, the "this" references inside it (such as "this.textToEcho") cannot be resolved:
	console.log("extractedEchoFunctionResult=" + extractedEchoFunctionResult);
	//debug in Chrome to see how this works
};

function echo(text) {
	return "Echo: " + text;
};