var twoWayBindingModule = angular.module("twoWayBindingApp", []);

/**
 * $scope is a JavaScript object containing all the variables declared in the HTML view directives
 * For example: declaring in the HTML view template a tag like <input ng-model="name"> will make it so that the variable $scope.name is bound to that input.
 * Any text typed by the user in this HTML <input> will be available in the $scope.name variable here, in the controller
 */
twoWayBindingModule.controller("twoWayBindingController", function($scope) {
	/*
	 * here, we initialize a variable in the data model, called "name" and having some text value
	 * The UI will automatically see any changes we programmatically do to this variable, here, in the controller
	 * Also, if the UI itself changes this variable, the controller will have available the latest value inputed for this variable, so we can use it in code, here. 
	 */
	$scope.name = "-input NE name here2-";
	
	//create an array and initialize it with some strings:
	$scope.productFamilies = ["ip10", "ip20"];
	
	//create a new empty array
	$scope.productNamesByFamily = [];
	//use this array as a key/value "map" by initializing each of the desired key values (ip10 and ip20 in this case)
	//each key is initialized to a new array, and that array itself is initialized upon declaration with some strings:
	$scope.productNamesByFamily["ip10"] = ["ip10c", "ip10e", "ip10g"];
	$scope.productNamesByFamily["ip20"] = ["ip20f", "ip20e", "ip20s"];
	
	//$scope.selectedProductFamily = "";
	
	$scope.selectedGroup = "1+0";
	$scope.groupsByFamily = [];
	$scope.groupsByFamily[undefined] = ["1+0"];
	$scope.groupsByFamily["ip10"] = ["1+0", "hsb", "mr"];
	$scope.groupsByFamily["ip20"] = ["1+0", "abc", "rpg"];
	
	//create a function named "submitNetworkElementData":
	//this function's purpose is to collect various variables' values form the model-view data model, and group them in a single object, that can than be used for other, non-UI related purposes
	//(send the data to the back-end, etc)
	function submitNetworkElementData() {
		
		//create a new object variable, named "networkElement". This variable will NOT be shared with the UI. As such, it is not necessary to add it to the model-view $scope variable:
		var networkElement = {};
		//add a field named "NAME" to the networkElement object, and initialize that field to the value from the "name" variable, from the model-view data model (from $scope):
		networkElement.NAME = $scope.name;
		//add another field named "PRODUCT_FAMILY" to the networkElement object, and initialize that field to the value from the "selectedProductFamily" variable, 
		//from the model-view data model (from $scope):
		networkElement.PRODUCT_FAMILY = $scope.selectedProductFamily;
		//add yet another field, and initialize it also with the value from some other variable in the data model:
		networkElement.PRODUCT_NAME = $scope.selectedProductName;
		
		networkElement.GROUP = $scope.selectedGroup;
		
		//here, we can sent the data from the networkElement object to the back-end, for persistence
		//for debugging purposes, we'll want (for now) to simply show all the fields and values from the networkElement in the UI, so we need to add it to some variable in the 
		//model-view data model:
		$scope.networkElementForDebug = networkElement;
	};
	
	//we want to be able to call the submitNetworkElementData function from the UI
	//as such, we need to expose this function to the UI, by adding it to some variable (named "performSubmit") in the model-view data model (to $scope):
	$scope.performSubmit = submitNetworkElementData;
});