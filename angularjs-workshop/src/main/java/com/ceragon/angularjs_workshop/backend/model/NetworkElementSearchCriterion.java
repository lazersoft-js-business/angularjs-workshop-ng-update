package com.ceragon.angularjs_workshop.backend.model;

public class NetworkElementSearchCriterion
{
    private String fieldName;
    private Object fieldValue;
    
    public String getFieldName()
    {
        return fieldName;
    }
    
    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }
    
    public Object getFieldValue()
    {
        return fieldValue;
    }
    
    public void setFieldValue(Object fieldValue)
    {
        this.fieldValue = fieldValue;
    }
}
