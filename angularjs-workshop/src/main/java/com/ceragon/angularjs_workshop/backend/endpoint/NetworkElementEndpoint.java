package com.ceragon.angularjs_workshop.backend.endpoint;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ceragon.angularjs_workshop.backend.model.CreateNetworkElementResponse;
import com.ceragon.angularjs_workshop.backend.model.NetworkElement;
import com.ceragon.angularjs_workshop.backend.model.NetworkElementSearchRequest;
import com.ceragon.angularjs_workshop.backend.model.NetworkElementSearchResponse;
import com.ceragon.angularjs_workshop.backend.model.UpdateNetworkElementResponse;
import com.ceragon.angularjs_workshop.backend.service.NetworkElementService;

@RestController
@RequestMapping(path = "/network-element")
public class NetworkElementEndpoint
{
    private final NetworkElementService networkElementService;
    
    @Autowired
    public NetworkElementEndpoint(NetworkElementService networkElementService)
    {
        this.networkElementService = networkElementService;
    }
    
    @RequestMapping(value = "/all",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<NetworkElement> getAll()
    {
        return networkElementService.getAll();
    }
    
    @RequestMapping(value = "/search",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public NetworkElementSearchResponse search(@RequestBody NetworkElementSearchRequest request)
    {
        return networkElementService.search(request);
    }
    
    @RequestMapping(value = "/create",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public CreateNetworkElementResponse create(@RequestBody NetworkElement networkElement)
    {
        return networkElementService.create(networkElement);
    }
    
    @RequestMapping(value = "/update",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public UpdateNetworkElementResponse update(@RequestBody NetworkElement networkElementUpdate)
    {
        return networkElementService.update(networkElementUpdate);
    }
    
    @RequestMapping(value = "/reset-data-to-original",
                    method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public void resetDataToOriginal()
    {
        networkElementService.resetDataToOriginal();
    }
    
    @RequestMapping(value = "/get-by-id",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public NetworkElement getById(@RequestParam(name = "ID", required = true) int id)
    {
        return networkElementService.getById(id);
    }
}
