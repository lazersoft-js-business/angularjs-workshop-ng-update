package com.ceragon.angularjs_workshop.backend.model;

import java.util.List;

public class NetworkElementSearchRequest
{
    private String                              sortField;
    private String                              sortOrder;
    private List<NetworkElementSearchCriterion> searchCriteria;
    private Integer                             from;
    private Integer                             size;
    
    public Integer getFrom()
    {
        return from;
    }
    
    public void setFrom(Integer from)
    {
        this.from = from;
    }
    
    public Integer getSize()
    {
        return size;
    }
    
    public void setSize(Integer size)
    {
        this.size = size;
    }
    
    public String getSortField()
    {
        return sortField;
    }
    
    public void setSortField(String sortField)
    {
        this.sortField = sortField;
    }
    
    public String getSortOrder()
    {
        return sortOrder;
    }
    
    public void setSortOrder(String sortOrder)
    {
        this.sortOrder = sortOrder;
    }
    
    public List<NetworkElementSearchCriterion> getSearchCriteria()
    {
        return searchCriteria;
    }
    
    public void setSearchCriteria(List<NetworkElementSearchCriterion> searchCriteria)
    {
        this.searchCriteria = searchCriteria;
    }
    
}
