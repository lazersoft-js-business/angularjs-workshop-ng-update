package com.ceragon.angularjs_workshop.backend.model;

import java.util.HashMap;

@SuppressWarnings("serial")
public class NetworkElement extends HashMap<String, Object>
{
    public enum Field
    {
     ID,
     NAME,
     IP,
     PRODUCT_FAMILY,
     PRODUCT_NAME,
     GROUP,
     SLOTS,
     LAST_UPDATE_TIMESTAMP,
     INSTALLATION_DATE
    }
    
    public void setFieldValue(Field field, Object value)
    {
        put(field.name(), value);
    }
    
    @SuppressWarnings("unchecked")
    public <T> T getFieldValue(Field field)
    {
        return (T) get(field.name());
    }
    
    @Override
    public int hashCode()
    {
        Integer id = getFieldValue(Field.ID);
        return id.hashCode();
    }
    
    @Override
    public boolean equals(Object other)
    {
        return this.getFieldValue(Field.ID).equals(((NetworkElement) other).getFieldValue(Field.ID));
    }
}
