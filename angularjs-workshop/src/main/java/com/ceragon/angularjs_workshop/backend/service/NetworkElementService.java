package com.ceragon.angularjs_workshop.backend.service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.ceragon.angularjs_workshop.backend.model.CreateNetworkElementResponse;
import com.ceragon.angularjs_workshop.backend.model.NetworkElement;
import com.ceragon.angularjs_workshop.backend.model.NetworkElement.Field;
import com.ceragon.angularjs_workshop.backend.model.NetworkElementSearchCriterion;
import com.ceragon.angularjs_workshop.backend.model.NetworkElementSearchRequest;
import com.ceragon.angularjs_workshop.backend.model.NetworkElementSearchResponse;
import com.ceragon.angularjs_workshop.backend.model.UpdateNetworkElementResponse;

import edu.emory.mathcs.backport.java.util.concurrent.atomic.AtomicInteger;

@Service
public class NetworkElementService
{
    private static final AtomicInteger   ID_SEQUENCE = new AtomicInteger(-1);
    
    private Map<Integer, NetworkElement> originalDataDb;
    private Map<Integer, NetworkElement> db;
    
    public NetworkElementService()
    {
        synchronized (this)
        {
            db = new HashMap<>();
            initDBData();
            originalDataDb = cloneMap(db);
        }
    }
    
    public synchronized void resetDataToOriginal()
    {
        db = cloneMap(originalDataDb);
    }
    
    public synchronized NetworkElement getById(int id)
    {
        return db.get(id);
    }
    
    public synchronized Collection<NetworkElement> getAll()
    {
        return db.values();
    }
    
    public synchronized NetworkElementSearchResponse search(NetworkElementSearchRequest request)
    {
        List<NetworkElement> sortedNEs = sort(request, db);
        filter(request, sortedNEs);
        int totalHits = sortedNEs.size();
        List<NetworkElement> paginatedNEs = paginate(request, sortedNEs);
        
        NetworkElementSearchResponse response = new NetworkElementSearchResponse();
        response.setResults(paginatedNEs);
        response.setTotalHits(totalHits);
        return response;
    }
    
    public synchronized CreateNetworkElementResponse create(NetworkElement networkElement)
    {
        validateIP(networkElement.getFieldValue(NetworkElement.Field.IP));
        
        networkElement.setFieldValue(NetworkElement.Field.ID, ID_SEQUENCE.incrementAndGet());
        storeNetworkElement(networkElement);
        
        CreateNetworkElementResponse response = new CreateNetworkElementResponse();
        response.setId(networkElement.getFieldValue(NetworkElement.Field.ID));
        return response;
    }
    
    public synchronized UpdateNetworkElementResponse update(NetworkElement networkElementUpdate)
    {
        validateIP(networkElementUpdate.getFieldValue(NetworkElement.Field.IP));
        
        UpdateNetworkElementResponse response = new UpdateNetworkElementResponse();
        
        NetworkElement originalNetworkElement = db.get(networkElementUpdate.getFieldValue(NetworkElement.Field.ID));
        if (originalNetworkElement == null)
        {
            response.setFoundEntryToUpdate(false);
            return response;
        }
        else
        {
            response.setFoundEntryToUpdate(true);
            originalNetworkElement.putAll(networkElementUpdate);
        }
        
        return response;
    }
    
    private void initDBData()
    {
        storeNetworkElement(createNetworkElement("My IP20 A", "192.168.66.68", "ip20a", "RPG", 16, getInstant(2018, 8, 17, 16, 52, 3)));
        storeNetworkElement(createNetworkElement("Some IP20 C", "192.168.66.69", "ip20c", "1+0", 8, getInstant(2018, 7, 12, 16, 43, 10)));
        storeNetworkElement(createNetworkElement("Device 1", "192.168.66.70", "ip20a", "1+0", 8, getInstant(2013, 3, 22, 18, 21, 17)));
        storeNetworkElement(createNetworkElement("NE 43", "192.168.66.71", "ip20n", "ABC", 16, getInstant(2015, 6, 21, 17, 11, 18)));
        
        storeNetworkElement(createNetworkElement("Something", "10.10.66.100", "ip10g", "HSB", 4, getInstant(2010, 1, 17, 16, 22, 5)));
        storeNetworkElement(createNetworkElement("AN IP10", "10.10.66.102", "ip10c", "1+0", 8, getInstant(2010, 7, 13, 11, 42, 11)));
        storeNetworkElement(createNetworkElement("IP10C 1", "10.10.66.104", "ip10c", "1+0", 8, getInstant(2009, 3, 21, 13, 31, 17)));
        storeNetworkElement(createNetworkElement("IP10G 1", "10.10.66.106", "ip10g", "MR", 4, getInstant(2008, 3, 22, 16, 13, 11)));
        
        storeNetworkElement(createNetworkElement("IP10G 2", "11.11.66.121", "ip10g", "1+0", 4, getInstant(2011, 1, 17, 16, 21, 5)));
        storeNetworkElement(createNetworkElement("My .124", "11.11.66.124", "ip10c", "1+0", 8, getInstant(2011, 6, 13, 15, 42, 12)));
        storeNetworkElement(createNetworkElement("IP10C aAA", "11.11.66.125", "ip10c", "MR", 8, getInstant(2009, 7, 22, 13, 30, 17)));
        storeNetworkElement(createNetworkElement("Some other name", "11.11.66.127", "ip10g", "HSB", 4, getInstant(2009, 3, 22, 13, 13, 15)));
    }
    
    private void storeNetworkElement(NetworkElement ne)
    {
        db.put(ne.getFieldValue(NetworkElement.Field.ID), ne);
    }
    
    private NetworkElement createNetworkElement(String name, String ip, String productName, String group, int slots, Instant lastUpdateTimeStamp)
    {
        NetworkElement ne = new NetworkElement();
        ne.setFieldValue(NetworkElement.Field.ID, ID_SEQUENCE.incrementAndGet());
        String productFamily = productName.contains("ip20") ? "ip20" : "ip10";
        ne.setFieldValue(Field.NAME, name);
        ne.setFieldValue(Field.IP, ip);
        ne.setFieldValue(Field.PRODUCT_FAMILY, productFamily);
        ne.setFieldValue(Field.PRODUCT_NAME, productName);
        ne.setFieldValue(Field.GROUP, group);
        ne.setFieldValue(Field.SLOTS, slots);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date(lastUpdateTimeStamp.toEpochMilli()));
        ne.setFieldValue(Field.LAST_UPDATE_TIMESTAMP, utcTime);
        return ne;
    }
    
    private Instant getInstant(int year, int month, int day, int hour, int minute, int second)
    {
        return LocalDateTime.of(year, month, day, hour, minute, second).toInstant(ZoneOffset.UTC);
    }
    
    private <K, V> Map<K, V> cloneMap(Map<K, V> originalMap)
    {
        Map<K, V> cloneMap = new HashMap<>();
        
        for (K originalKey : originalMap.keySet())
        {
            cloneMap.put(originalKey, originalMap.get(originalKey));
        }
        
        return cloneMap;
    }
    
    private List<NetworkElement> sort(NetworkElementSearchRequest request, Map<Integer, NetworkElement> db)
    {
        Map<Integer, NetworkElement> dbClone = cloneMap(db);
        List<NetworkElement> allNEs = new ArrayList<>(dbClone.values());
        if (request.getSortField() != null && !request.getSortField().isEmpty())
        {
            NetworkElement.Field sortField = NetworkElement.Field.valueOf(request.getSortField());
            String sortOrder = request.getSortOrder() == null || request.getSortOrder().isEmpty() ? "asc" : request.getSortOrder();
            allNEs.sort((left, right) -> {
                Comparable<Object> leftValue = left.getFieldValue(sortField);
                Comparable<Object> rightValue = right.getFieldValue(sortField);
                if (sortOrder.equals("asc"))
                {
                    return leftValue.compareTo(rightValue);
                }
                else if (sortOrder.equals("desc"))
                {
                    return rightValue.compareTo(leftValue);
                }
                throw new RuntimeException("Unknown sort order: " + sortOrder + " Only asc/desc allowed.");
            });
        }
        
        return allNEs;
    }
    
    @SuppressWarnings("unchecked")
    private void filter(NetworkElementSearchRequest request, List<NetworkElement> nesToFilter)
    {
        if (request.getSearchCriteria() == null || request.getSearchCriteria().isEmpty())
        {
            return;
        }
        
        Iterator<NetworkElement> it = nesToFilter.iterator();
        outer: while (it.hasNext())
        {
            NetworkElement ne = it.next();
            for (NetworkElementSearchCriterion searchCriterion : request.getSearchCriteria())
            {
                Comparable<Object> fieldValue = ne.getFieldValue(NetworkElement.Field.valueOf(searchCriterion.getFieldName()));
                Comparable<Object> searchCriterionValue = (Comparable<Object>) searchCriterion.getFieldValue();
                if (!searchCriterionValue.equals(fieldValue))
                {
                    it.remove();
                    continue outer;
                }
            }
        }
    }
    
    private List<NetworkElement> paginate(NetworkElementSearchRequest request, List<NetworkElement> nesToPaginate)
    {
        if (request.getFrom() == null)
        {
            return nesToPaginate;
        }
        if (request.getFrom() >= nesToPaginate.size())
        {
            return nesToPaginate;
        }
        if (request.getFrom() < 0)
        {
            return nesToPaginate;
        }
        if (request.getSize() <= 0)
        {
            return nesToPaginate;
        }
        
        int to = Math.min(nesToPaginate.size(), request.getFrom() + request.getSize());
        return nesToPaginate.subList(request.getFrom(), to);
    }
    
    private void validateIP(final String ip)
    {
        if (ip == null)
        {
            throw new IPValidationException("IP is empty (null)");
        }
        
        boolean valid = Pattern.compile("^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$").matcher(ip).matches();
        if (!valid)
        {
            throw new IPValidationException(ip + " is not a valid IP value");
        }
    }
}
