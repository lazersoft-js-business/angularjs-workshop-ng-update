package com.ceragon.angularjs_workshop.backend.endpoint;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/echo")
public class EchoEndpoint
{
    @RequestMapping(value = "/{text}",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public String echo(@PathVariable("text") String text)
    {
        return "ECHO: " + text;
    }
}
