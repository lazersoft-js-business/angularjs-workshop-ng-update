package com.ceragon.angularjs_workshop.backend.endpoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/javascript")
public class StaticResourcesController
{
    @RequestMapping(method = RequestMethod.GET, path = {"/**/*.html", "/**/*.css", "/**/*.js"})
    public String getStaticResource(HttpServletRequest request, HttpServletResponse response)
    {
        return "bla.html"; 
    }
}
