package com.ceragon.angularjs_workshop.backend.model;

import java.util.Collection;

public class NetworkElementSearchResponse
{
    private Collection<NetworkElement> results;
    private int                        totalHits;
    
    public Collection<NetworkElement> getResults()
    {
        return results;
    }
    
    public void setResults(Collection<NetworkElement> results)
    {
        this.results = results;
    }
    
    public int getTotalHits()
    {
        return totalHits;
    }
    
    public void setTotalHits(int totalHits)
    {
        this.totalHits = totalHits;
    }
    
}
