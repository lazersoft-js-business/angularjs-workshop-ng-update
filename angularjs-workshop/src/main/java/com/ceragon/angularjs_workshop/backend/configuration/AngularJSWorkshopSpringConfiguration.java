package com.ceragon.angularjs_workshop.backend.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.ceragon.angularjs_workshop.backend")
public class AngularJSWorkshopSpringConfiguration
{
    
}
