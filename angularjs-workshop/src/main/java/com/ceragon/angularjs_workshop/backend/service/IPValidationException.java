package com.ceragon.angularjs_workshop.backend.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason="Invalid IP value")
public class IPValidationException extends RuntimeException {

	public IPValidationException(String message) {
		super(message);
	}
}
