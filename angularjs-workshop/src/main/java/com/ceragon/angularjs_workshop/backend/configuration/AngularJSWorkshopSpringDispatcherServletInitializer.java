package com.ceragon.angularjs_workshop.backend.configuration;

import javax.servlet.Filter;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class AngularJSWorkshopSpringDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer
{
    
    @Override
    protected Class<?>[] getRootConfigClasses()
    {
        return new Class[] { AngularJSWorkshopSpringConfiguration.class };
    }
    
    @Override
    protected Class<?>[] getServletConfigClasses()
    {
        return null;
    }
    
    @Override
    protected String[] getServletMappings()
    {
        return new String[] { "/back-end/*" };
    }
    
    @Override
    protected Filter[] getServletFilters()
    {
        return new Filter[] { new CORSFilter() };
    }
    
}
