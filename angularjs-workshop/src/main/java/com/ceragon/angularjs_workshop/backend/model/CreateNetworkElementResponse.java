package com.ceragon.angularjs_workshop.backend.model;

public class CreateNetworkElementResponse
{
    public Integer id;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }
}
