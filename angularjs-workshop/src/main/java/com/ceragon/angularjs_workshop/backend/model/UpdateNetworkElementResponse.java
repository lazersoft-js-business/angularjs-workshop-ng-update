package com.ceragon.angularjs_workshop.backend.model;

public class UpdateNetworkElementResponse
{
    private boolean foundEntryToUpdate;
    
    public boolean isFoundEntryToUpdate()
    {
        return foundEntryToUpdate;
    }
    
    public void setFoundEntryToUpdate(boolean foundEntryToUpdate)
    {
        this.foundEntryToUpdate = foundEntryToUpdate;
    }
    
}
