package end2end.day1.two_way_binding;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import end2end.testSupport.BaseWebTest;
import end2end.testSupport.WebElementFinder;
import end2end.testSupport.WebPage;

public class TwoWayBindingTest extends BaseWebTest
{
    
    @Test
    public void testProductFamilySelection()
    {
        WebPage twoWayBindingPage = new WebPage(getBaseURL(), "/javascript/day1/two_way_binding/twoWayBinding.html", getWebDriver());
        twoWayBindingPage.navigate();
        
        WebElementFinder webElementFinder = new WebElementFinder(getWebDriver());
        
        WebElement productFamilySelect = webElementFinder.find(By.xpath("//select[1]"));
        productFamilySelect.click();
        List<WebElement> productFamilyOptions = productFamilySelect.findElements(By.xpath("//select[1]/option"));
        assertTrue(productFamilyOptions.size() > 1);        
        productFamilyOptions.get(1).click();
        waitSeconds(1);
        
        WebElement productNameSelect = webElementFinder.find(By.xpath("//select[2]"));
        productNameSelect.click();
        List<WebElement> productNameOptions = productNameSelect.findElements(By.xpath("//select[2]/option"));
        assertTrue(productNameOptions.size() > 1);       
        
        for(int i = 1; i < productNameOptions.size(); i++) {
            String optionText = productNameOptions.get(i).getText();
            assertTrue(optionText.contains(productFamilyOptions.get(1).getText()));
        }
    }
    
    @Test
    public void testSubmit()
    {
        WebPage twoWayBindingPage = new WebPage(getBaseURL(), "/javascript/day1/two_way_binding/twoWayBinding.html", getWebDriver());
        twoWayBindingPage.navigate();
        
        WebElementFinder webElementFinder = new WebElementFinder(getWebDriver());
        
        WebElement nameInput = webElementFinder.find(By.xpath("//input[1]"));
        nameInput.clear();
        nameInput.sendKeys("neName");
        
        WebElement productFamilySelect = webElementFinder.find(By.xpath("//select[1]"));
        productFamilySelect.click();
        List<WebElement> productFamilyOptions = productFamilySelect.findElements(By.xpath("//select[1]/option"));
        assertTrue(productFamilyOptions.size() > 1);        
        productFamilyOptions.get(1).click();
        waitSeconds(1);
        
        WebElement productNameSelect = webElementFinder.find(By.xpath("//select[2]"));
        productNameSelect.click();
        List<WebElement> productNameOptions = productNameSelect.findElements(By.xpath("//select[2]/option"));
        productNameOptions.get(1).click();
        
        WebElement submitButton = webElementFinder.find(By.xpath("//input[2]"));
        submitButton.click();
        
        getWebDriver().getPageSource().contains("\"NAME\":\"neName\"");
        getWebDriver().getPageSource().contains("\"PRODUCT_FAMILY\":\"" +  productFamilyOptions.get(1).getText() + "\"");
        getWebDriver().getPageSource().contains("\"PRODUCT_NAME\":\"" +  productNameOptions.get(1).getText() + "\"");
    }
}
