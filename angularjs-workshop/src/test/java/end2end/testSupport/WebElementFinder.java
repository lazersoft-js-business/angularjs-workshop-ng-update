package end2end.testSupport;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebElementFinder
{
    private final WebDriver webDriver;
    
    public WebElementFinder(WebDriver webDriver)
    {
        this.webDriver = webDriver;
    }
    
    public WebDriver getWebDriver()
    {
        return webDriver;
    }
    
    public void waitSeconds(float seconds)
    {
        try
        {
            Thread.sleep((long) (seconds * 1000));
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public List<WebElement> findAll(By by)
    {
        List<WebElement> webElements = null;
        try
        {
            webElements = getWebDriver().findElements(by);
        }
        catch (NoSuchElementException e)
        {
            webElements = new ArrayList<>();
        }
        return webElements;
    }
    
    public WebElement find(By by)
    {
        WebElement webElement = null;
        try
        {
            webElement = getWebDriver().findElement(by);
        }
        catch (NoSuchElementException e)
        {
            // nothing to do
        }
        return webElement;
    }
    
    public boolean exists(By by)
    {
        return find(by) != null;
    }
}
