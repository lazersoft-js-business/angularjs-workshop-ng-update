package end2end.testSupport;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.naming.resources.VirtualDirContext;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BaseWebTest
{
    private int       tomcatTestPort = 9876;
    private Tomcat    tomcat;
    private String    tomcatBaseDir  = "target/tomcat_for_tests";
    private String    baseURL        = "http://localhost:" + tomcatTestPort + "/ang";
    private WebDriver webDriver;
    
    @Before
    public void initTest() throws Throwable
    {
        initTomcat();
        initWebDriver();
    }
    
    @After
    public void cleanUpTest() throws Throwable
    {
        closeWebDriver();
        closeTomcat();
    }
    
    public WebDriver getWebDriver()
    {
        return webDriver;
    }
    
    public String getBaseURL()
    {
        return this.baseURL;
    }
    
    public void waitSeconds(float seconds)
    {
        try
        {
            Thread.sleep((long) (seconds * 1000));
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private void initTomcat() throws LifecycleException, ServletException
    {
        String webappDirLocation = "src/main/webapp/";
        tomcat = new Tomcat();
        tomcat.setPort(tomcatTestPort);
        tomcat.setBaseDir(tomcatBaseDir);
        
        StandardContext ctx = (StandardContext) tomcat.addWebapp("/ang",
                                                                 new File(webappDirLocation).getAbsolutePath());
        
        // declare an alternate location for your "WEB-INF/classes" dir:
        File additionWebInfClasses = new File("target/classes");
        VirtualDirContext resources = new VirtualDirContext();
        resources.setExtraResourcePaths("/WEB-INF/classes=" + additionWebInfClasses);
        ctx.setResources(resources);
        
        tomcat.start();
        System.out.println("\t +++ Tomcat started on port: " + tomcatTestPort);
        // tomcat.getServer().await();
    }
    
    private void closeTomcat() throws LifecycleException
    {
        tomcat.stop();
        tomcat.destroy();
        System.out.println("\t --- Tomcat closed");
    }
    
    private void initWebDriver()
    {
        String selectedBrowser = System.getProperty("lma.test.end2end.browser");
        if (selectedBrowser == null)
        {
            System.out.println("Selected end2end browser not set via env \"angular_workshop.test.end2end.browser\". Using default (chrome)");
            selectedBrowser = "chrome";
            // selectedBrowser = "firefox";
        }
        
        switch (selectedBrowser)
        {
            case "chrome":
            {
                webDriver = initChromeWebDriver();
                break;
            }
            case "firefox":
            {
                webDriver = initFirefoxWebDriver();
                break;
            }
        }
        
        System.out.println("\t +++ Initied webDriver for browser: " + selectedBrowser);
    }
    
    private void closeWebDriver() throws InterruptedException
    {
        Thread.sleep(3000);
        webDriver.close();
        // quit is needed to also kill the xxxdriver.exe process:
        try
        {
            webDriver.quit();
        }
        catch (Throwable t)
        {
            System.out.println(t.getMessage());
        }
        System.out.println("\t --- WebDriver closed");
    }
    
    @SuppressWarnings("deprecation")
    private WebDriver initChromeWebDriver()
    {
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver.exe");
        
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        capabilities.setCapability("pageLoadStrategy", "none");
        
        ChromeDriver webDriver = new ChromeDriver(capabilities);
        
        webDriver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
        webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        
        return webDriver;
    }
    
    private WebDriver initFirefoxWebDriver()
    {
        System.setProperty("webdriver.gecko.driver", "./src/test/resources/geckodriver.exe");
        FirefoxDriver webDriver = new FirefoxDriver();
        
        webDriver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
        webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        
        webDriver.manage().window().maximize();
        
        return webDriver;
    }
}
