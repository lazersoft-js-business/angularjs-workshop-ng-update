package end2end.testSupport;

import org.openqa.selenium.WebDriver;

public class WebPage
{
    private final String    baseURL;
    private final String    pageURLSuffix;
    private final WebDriver webDriver;
    
    public WebPage(String baseURL, WebDriver webDriver)
    {
        this(baseURL, null, webDriver);
    }
    
    public WebPage(String baseURL, String pageURLSuffix, WebDriver webDriver)
    {
        this.baseURL = baseURL;
        this.pageURLSuffix = pageURLSuffix.startsWith("/") ? pageURLSuffix : "/" + pageURLSuffix;
        this.webDriver = webDriver;
    }
    
    public String getBaseURL()
    {
        return baseURL;
    }
    
    public String getPageURLSuffix()
    {
        return pageURLSuffix;
    }

    public WebDriver getWebDriver()
    {
        return webDriver;
    }
    
    public void navigate() {
        webDriver.navigate().to(baseURL + pageURLSuffix);
        waitSeconds(3);
    }
    
    public void waitSeconds(float seconds) {
        try
        {
            Thread.sleep((long) (seconds * 1000));
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
